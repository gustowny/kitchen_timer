//
// Triple kitchen timer for Aurduino.
//
// It handles three countdown timers (A, B, C) simultaniously.
// Each timer has one TM1637-based 7-segmet led display and two push buttons.
// One common piezo buzzer is used to sound an alarm when any timer reach 00:00.
// Button 1 starts, pauses (when clicked) and resets (when hold) the timer.
// Button 2 increases the time 1 minute (when clicked) or 10 min/s (when hold).
//
// Used libraries can be found at https://bitbucket.org/gustowny/
//
// Author   : gustowny@gmail.com
// License  : see LICENSE.txt
//

#include "TM1637.h"
#include "ledtimer.h"
#include "pullupbutton.h"
#include "buzzer.h"
#include "notes.h"

//--------------------------------------------------------------------------
// Pins
//--------------------------------------------------------------------------

const int pinBuzzer       = 2;

const int pinButtonA1     = 3;
const int pinButtonA2     = 4;
const int pinButtonB1     = 5;
const int pinButtonB2     = 6;
const int pinButtonC1     = 7;
const int pinButtonC2     = 8;

const int pinDisplayA_CLK = 9;
const int pinDisplayA_DIO = 10;
const int pinDisplayB_CLK = 11;
const int pinDisplayB_DIO = 12;
const int pinDisplayC_CLK = 0;
const int pinDisplayC_DIO = 1;

//--------------------------------------------------------------------------
// Brightness settings
//--------------------------------------------------------------------------

const int intensityGreen  = 7; // 7 = max
const int intensityRed    = 3;
const int intensityYellow = 7;

//--------------------------------------------------------------------------
// Global objects
//--------------------------------------------------------------------------

Buzzer        g_buzzer;

TM1637        g_ledDisplayA;
TM1637        g_ledDisplayB;
TM1637        g_ledDisplayC;

LEDTimer      g_timerA(g_ledDisplayA, g_buzzer);
LEDTimer      g_timerB(g_ledDisplayB, g_buzzer);
LEDTimer      g_timerC(g_ledDisplayC, g_buzzer);

PullupButton  g_buttonA1;
PullupButton  g_buttonA2;
PullupButton  g_buttonB1;
PullupButton  g_buttonB2;
PullupButton  g_buttonC1;
PullupButton  g_buttonC2;

//--------------------------------------------------------------------------
// Alarm melody
//--------------------------------------------------------------------------

const char g_imperialMarchNotes[]     = "AAAfCAfCA ";
const char g_imperialMarchDurations[] = "4443143188";

//--------------------------------------------------------------------------
// Setup
//--------------------------------------------------------------------------

void setup() 
{
  g_ledDisplayA.begin(pinDisplayA_DIO, pinDisplayA_CLK);
  g_ledDisplayA.setIntensity(intensityRed);
  g_ledDisplayA.displayOn();

  g_ledDisplayB.begin(pinDisplayB_DIO, pinDisplayB_CLK);
  g_ledDisplayB.setIntensity(intensityGreen);
  g_ledDisplayB.displayOn();

  g_ledDisplayC.begin(pinDisplayC_DIO, pinDisplayC_CLK);
  g_ledDisplayC.setIntensity(intensityYellow);
  g_ledDisplayC.displayOn();

  g_timerA.setTime(05,00);
  g_timerB.setTime(10,00);
  g_timerC.setTime(15,00);
  
  g_buttonA1.begin(pinButtonA1);  
  g_buttonA2.begin(pinButtonA2);  
  g_buttonB1.begin(pinButtonB1);  
  g_buttonB2.begin(pinButtonB2);
  g_buttonC1.begin(pinButtonC1);  
  g_buttonC2.begin(pinButtonC2);
  
  g_buzzer.begin(pinBuzzer);
  g_buzzer.setMelody(g_imperialMarchNotes, g_imperialMarchDurations);  
}

//--------------------------------------------------------------------------
// Loop
//--------------------------------------------------------------------------

void loop() 
{
  g_timerA.tick(g_buttonA1.scan(), g_buttonA2.scan());
  g_timerB.tick(g_buttonB1.scan(), g_buttonB2.scan());
  g_timerC.tick(g_buttonC1.scan(), g_buttonC2.scan());
  
  g_buzzer.tick();

  delay(10);
}
